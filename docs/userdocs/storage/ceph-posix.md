:exclamation:We now have storage located in several geographic regions. Make sure you use the [right compute nodes](#using-the-right-region-for-your-pod) to ensure the optimal speed accessing it!

:exclamation:Installing `conda` and `pip` packages on all CephFS (shared) filesystems is strictly prohibited!


## Cleaning up

**Please [purge](/userdocs/storage/purging/) any data you don't need. We're not an archival storage, and can only store the data actively used for computations.**
## Posix volumes


Persistent data in kubernetes comes in a form of [Persistent Volumes (PV)](https://kubernetes.io/docs/concepts/storage/persistent-volumes/), which can only be seen by cluster admins. To request a PV, you have to create a [PersistentVolumeClaim (PVC)](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims) of a supported [StorageClass](https://kubernetes.io/docs/concepts/storage/storage-classes/) in your namespace, which will allocate storage for you.

### Ceph filesystems data use

<div id="observablehq-plot-087dc8ea"></div>
<p>Credit: <a href="https://observablehq.com/d/b9c19d9f7c57a186">Ceph data usage by Dmitry Mishin</a></p>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@observablehq/inspector@5/dist/inspector.css">
<script type="module">
import {Runtime, Inspector} from "https://cdn.jsdelivr.net/npm/@observablehq/runtime@5/dist/runtime.js";
import define from "https://api.observablehq.com/d/b9c19d9f7c57a186.js?v=3";
new Runtime().module(define, name => {
  if (name === "plot") return new Inspector(document.querySelector("#observablehq-plot-087dc8ea"));
});
</script>

### Currently available storageClasses:

<table>
  <thead>
    <tr class="header">
      <th>StorageClass</th>
      <th>Filesystem Type</th>
      <th>Region</th>
      <th>AccessModes</th>
      <th><span style="color:red">Restrictions</span></th>
      <th>Storage Type</th>
      <th>Size</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td markdown="span">rook-cephfs</td>
      <td markdown="span">CephFS</td>
      <td markdown="span">US West</td>
      <td markdown="span">ReadWriteMany</td>
      <td markdown="span"></td>
      <td markdown="span">Spinning drives with NVME meta</td>
      <td markdown="span">2.5 PB</td>
    </tr>
    <tr>
      <td markdown="span">rook-cephfs-central</td>
      <td markdown="span">CephFS</td>
      <td markdown="span">US Central</td>
      <td markdown="span">ReadWriteMany</td>
      <td markdown="span"></td>
      <td markdown="span">Spinning drives with NVME meta</td>
      <td markdown="span">1 PB</td>
    </tr>
    <tr>
      <td markdown="span">rook-cephfs-east</td>
      <td markdown="span">CephFS</td>
      <td markdown="span">US East</td>
      <td markdown="span">ReadWriteMany</td>
      <td markdown="span"></td>
      <td markdown="span">Mixed</td>
      <td markdown="span">1 PB</td>
    </tr>
    <tr>
      <td markdown="span">rook-cephfs-south-east</td>
      <td markdown="span">CephFS</td>
      <td markdown="span">US South East</td>
      <td markdown="span">ReadWriteMany</td>
      <td markdown="span"></td>
      <td markdown="span">Spinning drives with NVME meta</td>
      <td markdown="span">600 TB</td>
    </tr>
    <tr>
      <td markdown="span">rook-cephfs-pacific</td>
      <td markdown="span">CephFS</td>
      <td markdown="span">Hawaii+Guam</td>
      <td markdown="span">ReadWriteMany</td>
      <td markdown="span"></td>
      <td markdown="span">Spinning drives with NVME meta</td>
      <td markdown="span">384TB</td>
    </tr>
    <tr>
      <td markdown="span">rook-cephfs-haosu</td>
      <td markdown="span">CephFS</td>
      <td markdown="span">US West (local)</td>
      <td markdown="span">ReadWriteMany</td>
      <td markdown="span"><span style="color:red">Hao Su and Ravi cluster</span></td>
      <td markdown="span">NVME</td>
      <td markdown="span">131 TB</td>
    </tr>
    <tr>
      <td markdown="span">beegfs</td>
      <td markdown="span">BeeGFS</td>
      <td markdown="span">US West</td>
      <td markdown="span">ReadWriteMany</td>
      <td markdown="span"></td>
      <td markdown="span"></td>
      <td markdown="span">2PB</td>
    </tr>
    <tr>
      <td markdown="span">rook-ceph-block (*default*)</td>
      <td markdown="span">RBD</td>
      <td markdown="span">US West</td>
      <td markdown="span">ReadWriteOnce</td>
      <td markdown="span"></td>
      <td markdown="span">Spinning drives with NVME meta</td>
      <td markdown="span">2.5 PB</td>
    </tr>
    <tr>
      <td markdown="span">rook-ceph-block-east</td>
      <td markdown="span">RBD</td>
      <td markdown="span">US East</td>
      <td markdown="span">ReadWriteOnce</td>
      <td markdown="span"></td>
      <td markdown="span">Mixed</td>
      <td markdown="span">1 PB</td>
    </tr>
    <tr>
      <td markdown="span">rook-ceph-block-south-east</td>
      <td markdown="span">RBD</td>
      <td markdown="span">US South East</td>
      <td markdown="span">ReadWriteOnce</td>
      <td markdown="span"></td>
      <td markdown="span">Spinning drives with NVME meta</td>
      <td markdown="span">600 TB</td>
    </tr>
    <tr>
      <td markdown="span">rook-ceph-block-pacific</td>
      <td markdown="span">RBD</td>
      <td markdown="span">Hawaii+Guam</td>
      <td markdown="span">ReadWriteOnce</td>
      <td markdown="span"></td>
      <td markdown="span">Spinning drives with NVME meta</td>
      <td markdown="span">384 TB</td>
    </tr>
    <tr>
      <td markdown="span">rook-ceph-block-central</td>
      <td markdown="span">RBD</td>
      <td markdown="span">US Central</td>
      <td markdown="span">ReadWriteOnce</td>
      <td markdown="span"></td>
      <td markdown="span">Spinning drives with NVME meta</td>
      <td markdown="span">1 PB</td>
    </tr>
    <tr>
      <td markdown="span">seaweedfs-storage-hdd</td>
      <td markdown="span">SeaweedFS</td>
      <td markdown="span">US West</td>
      <td markdown="span">ReadWriteMany</td>
      <td markdown="span"></td>
      <td markdown="span">Spinning drives</td>
      <td markdown="span">24 TB</td>
    </tr>
    <tr>
      <td markdown="span">seaweedfs-storage-nvme</td>
      <td markdown="span">SeaweedFS</td>
      <td markdown="span">US West</td>
      <td markdown="span">ReadWriteMany</td>
      <td markdown="span"></td>
      <td markdown="span">NVME</td>
      <td markdown="span">22 TB</td>
    </tr>
  </tbody>
</table>

Ceph shared filesystem (**CephFS**) is the primary way of storing data in nautilus and allows mounting same volumes from multiple PODs in parallel (*ReadWriteMany*). Same applies to the **BeegFS** mounts accessed using NFS.

Ceph block storage allows [**RBD** (Rados Block Devices)](https://docs.ceph.com/docs/master/rbd/) to be attached to a **single pod** at a time (*ReadWriteOnce*). Provides fastest access to the data, and is preferred for smaller (below 500GB) datasets, and all datasets not needing shared access from multiple pods.

### When to choose each type of filesystem

RBD (Rados Block Device) is similar to a normal hard drive as it implements block storage on top of Ceph, and can run many kinds of file I/O operations including small files, thus may accommodate conda/pip installation and code compilation. It can provide higher IOPS than CephFS, but overall read/write performance tends to be slower than CephFS because it is less parallelized. Optimal read/write performance can be achieved when using a program or library that supports [librados](https://docs.ceph.com/en/latest/rados/api/librados-intro/) or you code your own program using the library. Use this for housing databases or workloads that require quick response but not necessarily high read/write rates. It shares the storage pool with CephFS, therefore being the largest storage pool in Nautilus.

CephFS is a distibuted parallel filesystem which stores files as objects. It may not handle lots of small files rapidly because it has to use metadata servers for annotating the files. Thus, conda/pip and code compilation should not be performed over CephFS. However, it has a much higher read/write performance than RBD. CephFS has the largest storage pool in Nautilus, and thus it is suitable for workloads that deal with comparably larger files than RBD which requires high I/O performance, for example checkpoint files of various tasks. There is a per-file size limit of 16 TB in CephFS.

BeeGFS is also a distributed parallel filesystem but stores files in a slightly different way to CephFS. It also may not handle lots of small files rapidly, thus conda/pip and code compilation should not be performed here as well. Because of networking limitations to the storage cluster, the read/write performance is very low, and therefore it is mainly suitable for archival purposes of files that are used less frequently than those that are housed in CephFS and RBD.

[SeaweedFS](/userdocs/storage/seaweedfs/) is a new experimental filesystem that improves many issues that exist in other filesystems. It can handle both many small and large files efficiently while having high read/write performance. IOPS can also be quite high. However, the storage pool dedicated to SeaweedFS is comparably smaller to the CephFS and RBD cluster, thus there is a practical limitation on the storage space that may be used by one user.

Both SeaweedFS and Ceph provide an S3-compatible protocol interface (with a per-file size limit of 5 TiB). This is a native object storage protocol that can supply the maximum read/write performance. It uses the HTTP protocol instead of POSIX, if your tool supports the protocol instead of only POSIX file I/O. Many data science tools and libraries support the S3-compatible protocol as an alternative file I/O interface, and the protocol is well optimized for such purposes.

### Creating and mounting the PVC

Use kubectl to create the PVC:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: examplevol
spec:
  storageClassName: <required storage class>
  accessModes:
  - <access mode, f.e. ReadWriteOnce >
  resources:
    requests:
      storage: <volume size, f.e. 20Gi>
```

After you've created a PVC, you can see it's status (`kubectl get pvc pvc_name`). Once it has the Status `Bound`, you can attach it to your pod (claimName should match the name you gave your PVC):

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: vol-pod
spec:
  containers:
  - name: vol-container
    image: ubuntu
    args: ["sleep", "36500000"]
    volumeMounts:
    - mountPath: /examplevol
      name: examplevol
  restartPolicy: Never
  volumes:
    - name: examplevol
      persistentVolumeClaim:
        claimName: examplevol
```

### Using the right region for your pod


Latency is significantly affecting the I/O performance. If you want optimal access speed to Ceph, add the region affinity to your pod for the correct `region` (`us-east` or `us-west`):

```yaml
spec:
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: topology.kubernetes.io/region
            operator: In
            values:
            - us-west
```

You can list the nodes region label using: `kubectl get nodes -L topology.kubernetes.io/region`


### Volumes expanding


All ceph volumes created starting from December 2020 can be expanded by simply modifying the `storage` field of the PVC (either by using `kubectl edit pvc ...`, or `kubectl update -f updated_pvc_definition.yaml`)

For older ones, all `rook-ceph-block-*` and most `rook-cephfs-*` volumes can be expanded. If yours is not expanding, you can ask cluster admins to do it in manual mode.
