##[JupyterHub](https://jupyterhub-west.nrp-nautilus.io) on Nautilus
We provide the [JupyterHub](https://jupyterhub-west.nrp-nautilus.io) service running in our cluster, which is great
if you need to quickly run your workflow and do not want to learn any
kubernetes. Simply follow the above link (or [https://jupyterhub-west.nrp-nautilus.io](https://jupyterhub-west.nrp-nautilus.io)) and use your institutional credentials to login using CILogon.
Choose the hardware specs to spawn your instance.  Once authenticated you can run Jupyter notebooks as usual. 


Your persistent home folder initially will be limited to 5GB. If you need more, you can request it to be extended.
You can also request for [cephFS storage](/userdocs/storage/ceph-posix)  that is mounted to a shared disk space.
Please use this to store all the data, code and results that you would need for long experiments.


**NOTE:** your jupyter container will shut down 1hr after your browser disconnects from it. If you need your job to keep running, don't close the browser window.
You could either use a desktop with a persistent Internet connection or only use this for testing your code.

**NOTE:** Available images are described in the [scientific images section](/userdocs/running/sci-img/).

If you need more, proceed to [Step by Step Tensorflow with Jupyter](/userdocs/jupyter/jupyter-pod).