#### Get involved in NRP

1. Check out [fasterdata.es.net][esnet] to understand terms, tools, and capabilities common to R&E networking performance measurement and monitoring.
1. Review materials and videos from 2017 and 2018 National Research Platform Workshops at [http://prp.ucsd.edu/][prp]
1. Familiarize yourself with tools that the National Engagement Performance and
   Outreach Center at IU [EPOC][epoc] has to offer.
1. [Discover][discover] a regional Research & Education network in your area to collaborate with and plan for cyberinfrastructure capabilities in your region.
1. Deploy [perfSONAR node][perfsonar] and participate in at least one multi-institutional perfSONAR mesh that tests network performance
1. Engage with [XSEDE campus champions][xsede], CI facilitator, and/or other campus resources to identify campus scientific research and education drivers 
1. Participate in broader community  conversations:
   - Weekly NRP technical calls (open to all) every Thursday at 10:00 AM Pacific/1:00 PM Eastern. Sign up at [list info][list].  
   - check out the [NRP Matrix channel][matrix]
   - Campus Cyberinfrastructure Technical community calls and brown-bag lunches. Sign-up for cybinf-engr@es.net mail list hosted by Jason Zurawski at ESnet.




##### Docs, blogs and other links

- [Caltech Supercomputing Blog][caltechs]
- [Internet2 Global summit 2019][summit] 
- [Big Data Express][bigdata] - BigData Express research team, Fermilab Mar 8, 2019

[matrix]: /userdocs/start/contact/
[esnet]: http://fasterdata.es.net
[prp]: http://prp.ucsd.edu/
[epoc]: https://epoc.global
[discover]: https://www.thequilt.net
[perfsonar]: http://docs.perfsonar.net
[xsede]: https://www.xsede.org/web/site/community-engagement/campus-champions/current
[list]:   https://mailman.ucsd.edu/mailman/listinfo/prp-l.

[caltechs]: http://supercomputing.caltech.edu/blog/
[summit]: https://meetings.internet2.edu/2019-global-summit/detail/10005353/
[bigdata]: https://meetings.internet2.edu/media/medialibrary/2019/03/04/20190308-wu-netservices-bigdataex-v2.pdf
